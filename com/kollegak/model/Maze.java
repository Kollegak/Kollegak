package com.kollegak.model;

import java.io.BufferedReader;
import java.io.IOException;
import com.sun.javafx.scene.traversal.Direction;

public class Maze {

    private MazeField[][] MazeArray;
    private static int side;
    private Player Jaffa;
    private Replicator replicator;

    private Player Colonel;

    public Maze(BufferedReader br) throws IOException {

    	String[] parts = br.readLine().split(" ");
    	side = Integer.parseInt(parts[1]);

    	MazeArray = new MazeField[side][side];

    	int x = 0;
    	int y = 0;

    	// initializing the fields
		for (int i = 0; i < side * side; i++) {

			if (x == side) {	// x too large, next row
				x = 0; y++;
			}
			MazeArray[y][x] = mazeFieldReader(br.readLine());
			x++;
		}
    }

    public void setPlayer(String name, Player player){
        if(name.toLowerCase().equals("colonel"))Colonel=player;
        if(name.toLowerCase().equals("jaffa"))Jaffa=player;
    }

    public Replicator getReplicator() {
        return replicator;
    }

    public Player getColonel() {
        return Colonel;
    }

    public Player getJaffa() {
        return Jaffa;
    }

    public void setReplicator(Replicator replicator) {
        this.replicator = replicator;
    }

    public MazeField getMazeField(int x, int y) {
        return MazeArray[y][x];
    }

    public static int getSide(){
    	return side;
    }

	private MazeField mazeFieldReader(String line) {

		String[] parts = line.split(" ");
		int cubeCount = Integer.parseInt(parts[parts.length - 1]);

        return new MazeField(
                mazeFieldWall(parts[1]),
                mazeFieldWall(parts[2]),
                mazeFieldWall(parts[3]),
                mazeFieldWall(parts[4]),
                mazeFieldInteractable(parts),
                cubeCount
            );
	}

	private Wall mazeFieldWall(String type) {

		switch (type) {

		case "door":
			return new Door();
		case "normalWall":
			return new NormalWall();
		case "noWall":
			return new NoWall();
		case "specialWall":
			return new SpecialWall();
		default:
			return null;
		}
	}

    private Interactable mazeFieldInteractable(String[] parts){

    	if (parts.length == 7){		// not scale

            switch (parts[5]){		// the fifth element is the name of the interactable

            case "pit":
                return new Pit();
            case "ZPM":
                return new ZPM();
            default:
                return null;
            }
        } else {	// scale

        	// Door position
    		int x = Integer.parseInt(parts[8])-1;
    		int y = Integer.parseInt(parts[7])-1;
    		Coordinate coord = new Coordinate(x, y);
    		Direction dir = getDir(parts[9]);
    		int weight = Integer.parseInt(parts[6]);

    		return new Scale(this, weight, coord, dir);
    	}
    }

    private Direction getDir(String word) {

		switch (word) {

		case "up":
			return Direction.UP;
		case "down":
			return Direction.DOWN;
		case "left":
			return Direction.LEFT;
		case "right":
			return Direction.RIGHT;
		default:
			return null;
		}
  	}

    Coordinate otherField(Coordinate coordinate, Direction dir) {
        switch (dir) {
            // move UP/DOWN one step on the Y axis
            case UP:
                coordinate.setY(coordinate.getY() - 1);
                break;
            case DOWN:
                coordinate.setY(coordinate.getY() + 1);
                break;
            // move LEFT/RIGHT one step on the X axis
            case LEFT:
                coordinate.setX(coordinate.getX() - 1);
                break;
            case RIGHT:
                coordinate.setX(coordinate.getX() + 1);
                break;
            default:
                throw (new IllegalArgumentException("Invalid dir argument"));
        }
        return coordinate;
    }

    Direction otherDir(Direction dir){
        switch (dir) {
        // move UP/DOWN one step on the Y axis
        case UP:
            dir = Direction.DOWN;
            break;
        case DOWN:
            dir = Direction.UP;
            break;
        // move LEFT/RIGHT one step on the X axis
        case LEFT:
            dir = Direction.RIGHT;
            break;
        case RIGHT:
            dir = Direction.LEFT;
            break;
        default:
            throw (new IllegalArgumentException("Invalid dir argument"));
    }
    return dir;
    }
}
