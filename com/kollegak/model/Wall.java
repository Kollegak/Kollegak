package com.kollegak.model;

import com.kollegak.view.Drawable;
import com.sun.javafx.scene.traversal.Direction;

public interface Wall extends Drawable {
    //returns where objects get, if they try to pass through
    Coordinate moveThrough(Coordinate coordinate, Direction dir);
    //Reponsible for opening portal, if it can be open on the wall, return it's own coordinate.
    // If not, then returns the next wall to check. Null, if the bullet has been destroyed.
    Coordinate shoot(Coordinate pos, Direction dir);
}
