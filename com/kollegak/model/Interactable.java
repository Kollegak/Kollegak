package com.kollegak.model;

import com.kollegak.view.Drawable;

public interface Interactable extends Drawable{
//Interface for items
    //status booleans
      boolean collectable();
      boolean destroy();
//function called when stepping on the field, which contains the interactable
     void step(Actor actor);
//function called when leaving the field, which contains the interactable
     void leave(Actor actor);
}
