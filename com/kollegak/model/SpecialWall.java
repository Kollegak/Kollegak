package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class SpecialWall implements Wall {
    // We can never move through this object
    @Override
    public Coordinate moveThrough(Coordinate coordinate, Direction dir) {
        // Return unmodified coordinate
        return coordinate;
    }
    // We can always open a Portal on the object's position
    @Override
    public Coordinate shoot(Coordinate pos, Direction dir) {
        // Return unmodified coordinate
        return pos;
    }

    @Override
    public String getType() {
        return "specialwall";
    }
}
