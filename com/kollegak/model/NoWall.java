package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class NoWall implements Wall {

	// Always changes the coordinate in the given direction
	@Override
	public Coordinate moveThrough(Coordinate coordinate, Direction dir) {
		return modifyCoord(coordinate, dir);
	}

	@Override
	public Coordinate shoot(Coordinate coordinate, Direction dir) {
		Coordinate returnCoord = new Coordinate(coordinate.getX(), coordinate.getY());
		returnCoord = modifyCoord(returnCoord, dir);
		return returnCoord;
	}

	private Coordinate modifyCoord(Coordinate coordinate, Direction dir) {

		switch (dir) {
		case UP:
			coordinate.setY(coordinate.getY() - 1);
			break;
		case DOWN:
			coordinate.setY(coordinate.getY() + 1);
			break;
		case LEFT:
			coordinate.setX(coordinate.getX() - 1);
			break;
		case RIGHT:
			coordinate.setX(coordinate.getX() + 1);
			break;
		default:
			throw (new IllegalArgumentException("Invalid dir argument"));
		}
		return coordinate;
	}

	@Override
	public String getType() {
		return "nowall";
	}
}
