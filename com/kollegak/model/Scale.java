package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class Scale implements Interactable{

	private Maze maze;
	private Coordinate doorCoord;
	private Direction doorDir;
    private int playerNumber;
    private int boxNumber;
    private int reqNumber;
    private Door door1;
    private Door door2;
    private boolean init;

    public Scale(Maze maze, int reqNumber, Coordinate doorCoord, Direction doorDir)
    {
    	this.maze = maze;
        this.doorCoord=doorCoord;
        this.doorDir = doorDir;
        this.reqNumber=reqNumber;
        init = true;
        playerNumber=0;
    }

    @Override
    public boolean collectable() {
        return false;
    }

    @Override
    public boolean destroy() {
        return false;
    }

    @Override
    public void step(Actor actor) {
    	
    	if (init){	
    		
		door1 = (Door)maze.getMazeField(doorCoord.getX(), doorCoord.getY()).getWall(doorDir);
		Coordinate dCoord = maze.otherField(doorCoord, doorDir);
		Direction dDir = maze.otherDir(doorDir);
		door2 = (Door)maze.getMazeField(dCoord.getX(), dCoord.getY()).getWall(dDir);
		
		init = false;
    	}
    	
		switch (actor) {
		
		case Box:
			boxNumber++;
			if (boxNumber >= reqNumber) {
				door1.setOpen(true);
				door2.setOpen(true);
			}
			break;
		default:
            playerNumber++;
			door1.setOpen(true);
			door2.setOpen(true);
			break;
		}
	}

    @Override
    public void leave(Actor actor) {
		switch (actor) {
		case Box:
			boxNumber--;
			if (boxNumber < reqNumber) {
				door1.setOpen(false);
				door2.setOpen(false);
			}
			break;
		default:
            playerNumber--;
            if (playerNumber==0) {
                door1.setOpen(false);
                door2.setOpen(false);
            }
			break;
		}
	}

	@Override
	public String getType() {
		return "scale";
	}
}