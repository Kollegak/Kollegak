package com.kollegak.model;

public class Coordinate {
    //x and y coordinates are integer values
    private int x;
    private int y;

    //basic constructor
    public Coordinate() {
        this.x = 0;
        this.y = 0;
    }

    //basic constructor, which sets the coordinates
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //getter setter block
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public boolean equals(Coordinate coord) {
		if (x == coord.x && y == coord.y)
			return true;
		else return false;
    }

    @Override
    public String toString(){
        return "x: " + getX() + " y: " + getY() +" \n";
    }
}
