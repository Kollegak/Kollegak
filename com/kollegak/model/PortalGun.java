package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class PortalGun {

	private Portal PrimaryPortal;
	private Portal SecondaryPortal;
	private Maze maze;

    PortalGun(Maze maze){
        this.maze=maze;
    }

	public void shoot(String type, Coordinate pos, Direction dir) {
		// if invalid input, return
		if (!type.equals("primary") && !type.equals("secondary"))
			return;

		Coordinate newCoord = pos;
		boolean first = true;
		MazeField mf = null;

		// While no wall is hit, the bullet moves
		while ((newCoord != null && !newCoord.equals(pos)) || first) {

			first = false;
			pos = newCoord;
			mf = maze.getMazeField(pos.getX(), pos.getY());

			if (mf.hasReplicator()) { 	// shot blocked
				mf.setReplicator(false);
				maze.setReplicator(null);
				newCoord = null;
			} else
				newCoord = mf.getWall(dir).shoot(pos, dir);
		}

		// if shot destroyed, return
		if (newCoord == null)
			return;

		// if there is a portal with the same color, we destroy it
		if (type.equals("primary") && PrimaryPortal != null) {
			Coordinate c = PrimaryPortal.getCoordinate();
			MazeField oldLocation = maze.getMazeField(c.getX(), c.getY());
			oldLocation.setToSpecWall(PrimaryPortal.getDir());
		}

		// if there is a portal with the same color, we destroy it
		if (type.equals("secondary") && SecondaryPortal != null) {
			Coordinate c = SecondaryPortal.getCoordinate();
			MazeField oldLocation = maze.getMazeField(c.getX(), c.getY());
			oldLocation.setToSpecWall(SecondaryPortal.getDir());
		}

		// making portal
		if (type.equals("primary")) {
			PrimaryPortal = new Portal(newCoord, dir);
			mf.setToPortal(dir, PrimaryPortal);
		} else {
			SecondaryPortal = new Portal(newCoord, dir);
			mf.setToPortal(dir, SecondaryPortal);
		}

		// if two exists, we link them together
		if (PrimaryPortal != null && SecondaryPortal != null) {
			PrimaryPortal.setPairPortal(SecondaryPortal);
			SecondaryPortal.setPairPortal(PrimaryPortal);
        }
	}

    // portal getter
    public String getPortal(Coordinate pos, Direction dir){
        if (PrimaryPortal!=null)
            if (PrimaryPortal.getCoordinate().equals(pos) &&
                PrimaryPortal.getDir().equals(dir))
                    return "primary";
        if (SecondaryPortal!=null)
            if (SecondaryPortal.getCoordinate().equals(pos) &&
                SecondaryPortal.getDir().equals(dir))
                    return "secondary";
        return null;
    }
}
