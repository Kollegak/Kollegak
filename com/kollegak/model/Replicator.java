package com.kollegak.model;

import com.kollegak.controller.Main;
import com.kollegak.view.View;
import java.util.Random;
import com.sun.javafx.scene.traversal.Direction;

public class Replicator extends Thread {

	private Coordinate pos;
	private Direction dir;
	private Maze maze;
	View view;

	public Replicator(Coordinate pos, Direction dir, Maze maze, View view) {
		this.pos = pos;
		this.dir = dir;
		this.maze = maze;
		this.view=view;
		maze.getMazeField(pos.getX(), pos.getY()).setReplicator(true);
		view.repaint();
    }
	
	public Direction getDir() {
		return dir;
	}

	public Coordinate getPos() {
		return pos;
	}

	public void move() throws InterruptedException {

		Random rand = new Random();
		int random = rand.nextInt(4); // random number 0-3

		switch (random) {
			case 0:
				dir = Direction.UP;
				break;
			case 1:
				dir = Direction.LEFT;
				break;
			case 2:
				dir = Direction.DOWN;
				break;
			case 3:
				dir = Direction.RIGHT;
				break;
		}

    	MazeField currMf = maze.getMazeField(pos.getX(), pos.getY());	// current field
    	Coordinate newCoord = new Coordinate(pos.getX(), pos.getY());
    	newCoord = currMf.getWall(dir).moveThrough(newCoord, dir);		// trying to move

		if (!pos.equals(newCoord)) { 		// move not impossible
			MazeField newMf = maze.getMazeField(newCoord.getX(), newCoord.getY()); // target field
				
			if (newMf.getCube() == 0) 	// no cubes blocking the field
				step(newMf, currMf, newCoord);
		}
		view.repaint();
	}

	public void step(MazeField newMf, MazeField currMf, Coordinate newCoord) throws InterruptedException {

		pos = newCoord; 	// moving
		currMf.setReplicator(false);
		newMf.setReplicator(true);


		Interactable newInteractable = newMf.getInteractable();

		if (newInteractable != null && newInteractable.destroy()) { 	// pit
			newMf.setReplicator(false);
			newMf.removeInteractable();
            maze.setReplicator(null);
		}
		view.repaint();
		sleep(1000);
	}
	
	@Override
	public void run() {
		while (!(Main.getOver()) && maze.getMazeField(pos.getX(), pos.getY()).hasReplicator()){
				try {
					move();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
}
