package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class Door implements Wall {

    private boolean open;

    Door(){
        open = false;
    }

    public void setOpen(boolean isopen) {
        this.open = isopen;
    }

    // Function for setting the new coordinate
    // Coordinate(0,0) is situated in the left upper corner
    public Coordinate setReturnCoordinate(Coordinate coordinate, Direction dir){
        switch (dir) {
            case UP:
                coordinate.setY(coordinate.getY() - 1);
                break;
            case DOWN:
                coordinate.setY(coordinate.getY() + 1);
                break;
            case LEFT:
                coordinate.setX(coordinate.getX() - 1);
                break;
            case RIGHT:
                coordinate.setX(coordinate.getX() + 1);
                break;
            default:
                throw (new IllegalArgumentException("Invalid dir argument"));
        }
        return coordinate;
    }

    // Return a coordinate based on the door's status
    @Override
    public Coordinate moveThrough(Coordinate coordinate, Direction dir) {
    	
        Coordinate returnCoord = coordinate;	// door closed, same coordinate

        if (open) {
            returnCoord = setReturnCoordinate(coordinate, dir);	// set new coordinate according to direction
        }
        return returnCoord;
    }
    // Return a coordinate based on the door's status
    @Override
    public Coordinate shoot(Coordinate coordinate, Direction dir) {
        if (open){
             // door open, coordinate modified according to direction
    		Coordinate returnCoord = new Coordinate(coordinate.getX(), coordinate.getY());
    		returnCoord = setReturnCoordinate(returnCoord, dir);
    		return returnCoord;
        } else {
            return null;	// door closed, unable to shoot through
        }
    }

    @Override
    public String getType() {
        if (!open)
            return "door";
        return "opendoor";
    }
}
