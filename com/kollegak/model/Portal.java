package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class Portal implements Wall {

    private Coordinate Coordinate;
    private Direction Direction;
    private Portal pairPortal;
    private boolean hasPair;
    
    public Portal(Coordinate myPos, Direction dir) {
        this.Coordinate = myPos;
        this.Direction = dir;
	}

    //setting the coordinate for the associated portal
    public void setPairPortal(Portal pairPortal){
        this.pairPortal = pairPortal;
        hasPair = true;
    }

    //returns the coordinate, where things get, if they pass through the portal
    @Override
    public Coordinate moveThrough(Coordinate coordinate, Direction dir) {
        if (hasPair){
            //there is a pair, returning it's coordinate
            return pairPortal.getCoordinate();
        } else {
            //no pair, so returning it's own position
            return coordinate;
        }
    }

    //returns where the bullet goes if was shot here
    @Override
    public Coordinate shoot(Coordinate pos, Direction dir) {
        if (hasPair && pairPortal.getDir() != Direction){
            return pairPortal.getCoordinate();
        } else {
            return null;
        }
    }

    //getters area
    public Coordinate getCoordinate(){
        return  Coordinate;
    }

    public Direction getDir(){
        return Direction;
    }

    @Override
    public String getType() {
        return "portal";
    }
}
