package com.kollegak.model;

import com.sun.javafx.scene.traversal.Direction;

public class MazeField {

    private Wall LWall;
    private Wall RWall;
    private Wall UWall;
    private Wall DWall;
    private Interactable interactable;
    private int cubeCount;
    boolean replicator;

    public MazeField(Wall UWall, Wall RWall, Wall DWall, Wall LWall, Interactable interactable, int cubeCount) {
        this.UWall = UWall;
        this.RWall = RWall;
        this.DWall = DWall;
    	this.LWall = LWall;
        this.interactable = interactable;
        this.cubeCount = cubeCount;
    }

    public Wall getWall(Direction dir) {
        switch (dir){
            case UP:
                return UWall;
            case DOWN:
                return DWall;
            case LEFT:
                return LWall;
            case RIGHT:
                return RWall;
            default:
                throw new IllegalArgumentException("Invalid Direction at getWall");
        }
    }

    public void setToPortal(Direction dir, Portal p) {
        switch (dir){
            case UP:
                UWall = p;
                break;
            case DOWN:
                DWall = p;
                break;
            case LEFT:
                LWall = p;
                break;
            case RIGHT:
                RWall = p;
                break;
            default:
                throw new IllegalArgumentException("Invalid Direction at setToPortal");
        }
    }

    public void setToSpecWall(Direction dir) {
        switch (dir){
            case UP:
                UWall = new SpecialWall();
                break;
            case DOWN:
                DWall = new SpecialWall();
                break;
            case LEFT:
                LWall = new SpecialWall();
                break;
            case RIGHT:
                RWall = new SpecialWall();
                break;
            default:
                throw new IllegalArgumentException("Invalid Direction at setToSpecWall");
        }
    }

    public int getCube() {
        return cubeCount;
    }

    public void addCube() {
        cubeCount++;
    }
    
    public void pickCube() {
    	cubeCount--;
    }

    public Interactable getInteractable() {
        return interactable;
    }

    public void removeInteractable(){
    	interactable = null;
    }
    
    public void setInteractable(Interactable interactable){
    	this.interactable = interactable;
    }
    
    public boolean hasReplicator(){
    	return replicator;
    }
    
    public void setReplicator(boolean bl){
    	replicator = bl;
    }
}
