package com.kollegak.model;

public class Pit implements Interactable {
    
    // Always return false, because we can't pick up a Pit
    @Override
    public boolean collectable() {
        return false;
    }
    // Always returns true when called
    @Override
    public boolean destroy() {
        return true;
    }

    @Override
    public void step(Actor actor) {
    	// empty
    }
    // Empty function, as nothing can leave a pit
    @Override
    public void leave(Actor actor) {
    }

    @Override
    public String getType() {
        return "pit";
    }
}
