package com.kollegak.model;

public class ZPM implements Interactable {

    private static int ZPMCountColonel;
    private static int ZPMCountJaffa;
    private static int ZPMRemaining;

    public ZPM(){
        ZPMRemaining++;
    }

    @Override
    public boolean collectable() {
        return true;
    }

    @Override
    public boolean destroy() {
        return false;
    }

    @Override
    public void step(Actor actor) {
        switch (actor){
            case Colonel:
                ZPMCountColonel++;
                break;
            case Jaffa:
                ZPMCountJaffa++;
                break;
		default:
			break;
        }
        ZPMRemaining--;
    }

    @Override
    public void leave(Actor actor) {
        // empty
    }
    
    public static int GetColonelZPM(){
    	return ZPMCountColonel;
    }
    
    public static int GetJaffaZPM(){
    	return ZPMCountJaffa;
    }
    
    public static int GetZPMRemaining(){
    	return ZPMRemaining;
    }

    @Override
    public String getType() {
        return "zpm";
    }
}
