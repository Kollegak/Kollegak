package com.kollegak.model;

import java.util.Random;

import com.kollegak.view.View;
import com.sun.javafx.scene.traversal.Direction;

public class Player {

    private Actor actor;
    private Coordinate pos;
	private Direction dir;
    private Maze maze;
    private PortalGun gun;
    private boolean hasCube;
    private boolean over;
    private static String winner;
    private View view;

    public Player(Actor actor, Coordinate pos, Direction dir, Maze maze, View view) {
    	
    	this.actor = actor;
        this.pos = pos;
        this.dir = dir;
       	this.maze = maze;
        this.view=view;
        gun = new PortalGun(maze);
        winner=null;
		view.repaint();
	}

    //getter area
    public Actor getActor() {
        return actor;
    }

    public Direction getDir() {
        return dir;
    }

    public Coordinate getPos() {
        return pos;
    }

	public String getPortalType(Coordinate pos, Direction dir) {
        return gun.getPortal(pos, dir);
    }

    public boolean hasCube() {
        return hasCube;
    }

    public void move(String command) {
    	
    	boolean step = true;	// move or interact
    	
    	switch (command) {
    	
    	case "up":
    		dir = Direction.UP;
    		break;
    	case "left":
    		dir = Direction.LEFT;
    		break;
    	case "down":
    		dir = Direction.DOWN;
    		break;
    	case "right":
    		dir = Direction.RIGHT;
    		break;
    	case "interact":
    		step = false;	
    		break;
    	}
    	
    	MazeField currMf = maze.getMazeField(pos.getX(), pos.getY());	// current field
    	Coordinate newCoord = new Coordinate(pos.getX(), pos.getY());
    	newCoord = currMf.getWall(dir).moveThrough(newCoord, dir);		// trying to move
    	
    	if (!pos.equals(newCoord)) {			// move not impossible
    		
    		MazeField newMf = maze.getMazeField(newCoord.getX(), newCoord.getY());	// target field
    		
			if (step) {
				step(newMf, currMf, newCoord);
				
            } else if (hasCube) {
				putDownCube(newMf);
			} else {
				pickUpCube(newMf);
			}
		}
        view.repaint();
	}
    
    public void step(MazeField newMf, MazeField currMf, Coordinate newCoord) {
    	
		if (newMf.getCube() == 0) { 	// no cubes blocking the field
			pos = newCoord; 			// moving

			if (currMf.getInteractable() != null)
				currMf.getInteractable().leave(actor); 	// leaving current interactable

			Interactable newInteractable = newMf.getInteractable();

			if (newInteractable != null) {
				newInteractable.step(actor); 	// stepping on interactable

				if (newInteractable.collectable()) {
					newMf.removeInteractable(); 	// removing collectable

					if (actor == Actor.Colonel && ZPM.GetColonelZPM() % 2 == 0)
						createZPM();

					if (ZPM.GetZPMRemaining() == 0){  // game over
						over = true;

						if (ZPM.GetColonelZPM() > ZPM.GetJaffaZPM())
							winner = "Colonel";
						else
							winner = "Jaffa";
					}	
				}
				if (newInteractable.destroy()) { 			// player died
					over = true;

					if (actor == Actor.Colonel)
						winner = "Jaffa";
					else winner = "Colonel";
				}
			}
		}
        view.repaint();
    }

    public void putDownCube(MazeField newMf) {

		Interactable newInteractable = newMf.getInteractable();

		if (newInteractable == null) {
			hasCube = false;
			newMf.addCube();
		} else {
			// we got an interactable
			if (!newInteractable.collectable()) { // possible to put down
				hasCube = false;
				newInteractable.step(Actor.Box);

				if (!newInteractable.destroy()) { // not pit
					newMf.addCube();
				}
			}
		}
	}

    public void pickUpCube(MazeField newMf){
    	
		if (newMf.getCube() > 0){
			newMf.pickCube();		// decreasing cubeCount
			hasCube = true;
			
			Interactable newInteractable = newMf.getInteractable();
			
			if (newInteractable != null)
				newInteractable.leave(Actor.Box);	// cube picked up from interactable
		}
    }
    
    public void createZPM(){
    	
		Random rand = new Random();
		int num = maze.getSide();
		int x;
		int y;
		boolean ZPMCreated = false;
			
		while (!ZPMCreated) {
			
			x = rand.nextInt(num); // random number
			y = rand.nextInt(num);
			MazeField selected = maze.getMazeField(x, y);	// getting a field
			// creating a ZPM
			if (selected.getInteractable() == null) {
				
				selected.setInteractable(new ZPM());
				ZPMCreated = true;
			}
		}
	}

    public void shoot(String type){
    	gun.shoot(type, pos, dir);
        view.repaint();
    }
    
    public boolean returnOver(){
    	return over;
    }

    public static String returnWinner(){
        return winner;
    }
}
