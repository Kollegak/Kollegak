package com.kollegak.controller;

import com.kollegak.model.Player;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controller implements KeyListener{
	
    private Player colonel;
    private Player jaffa;

    //constructor sets the values
    Controller(Player colonel, Player jaffa)
    {
        this.colonel=colonel;
        this.jaffa=jaffa;
    }

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
        if (!Main.getOver()) {              //while the game is not over
            switch (key) {
                case KeyEvent.VK_UP:        //we check the input character (command) and parse it
                    colonel.move("up");
                    break;
                case KeyEvent.VK_DOWN:
                    colonel.move("down");
                    break;
                case KeyEvent.VK_RIGHT:
                    colonel.move("right");
                    break;
                case KeyEvent.VK_LEFT:
                    colonel.move("left");
                    break;
                case KeyEvent.VK_SPACE:
                    colonel.move("interact");
                    break;
                case KeyEvent.VK_W:
                    jaffa.move("up");
                    break;
                case KeyEvent.VK_A:
                    jaffa.move("left");
                    break;
                case KeyEvent.VK_D:
                    jaffa.move("right");
                    break;
                case KeyEvent.VK_S:
                    jaffa.move("down");
                    break;
                case KeyEvent.VK_F:
                    jaffa.move("interact");
                    break;
                case KeyEvent.VK_Q:
                    jaffa.shoot("primary");
                    break;
                case KeyEvent.VK_E:
                    jaffa.shoot("secondary");
                    break;
                case KeyEvent.VK_V:
                    colonel.shoot("primary");
                    break;
                case KeyEvent.VK_B:
                    colonel.shoot("secondary");
                    break;
            }
            if (colonel.returnOver()==true || jaffa.returnOver()==true)     //if the game is over we do not need more commands
                Main.setOver(true);
        }
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
