package com.kollegak.controller;

import com.kollegak.view.View;
import com.kollegak.model.*;
import com.sun.javafx.scene.traversal.Direction;

import java.io.*;

public class Main {

	//Switching basic settings to ready state
	private static Player jaffa;
	private static Player colonel;
	private static Replicator replicator;
	private static Maze maze;
	private static View view;
	private static Controller controller;
	private static volatile boolean over;

	public static void main(String[] args) throws IOException {
		BufferedReader br= new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/res/map.txt")));

        maze=new Maze(br);
        view=new View(maze);

        String line;
        while ((line=br.readLine())!=null) {
            String[] cmd = line.split(" ");
            if (cmd[0].toLowerCase().equals("spawn")) spawn(cmd);
        }

        br.close();

        over = false;

        maze.setPlayer("colonel", colonel);
        maze.setPlayer("jaffa", jaffa);
        maze.setReplicator(replicator);

		controller=new Controller(colonel,jaffa);
		view.addKeyListener(controller);
		
		replicator.run();
	}


    public static void spawn(String[] cmd) {

		// spawn position
		int x = Integer.parseInt(cmd[3])-1;
		int y = Integer.parseInt(cmd[2])-1;

		// spawn direction
		Direction dir = null;
		if (cmd[4].toLowerCase().equals("up"))
			dir = Direction.UP;
		if (cmd[4].toLowerCase().equals("down"))
			dir = Direction.DOWN;
		if (cmd[4].toLowerCase().equals("right"))
			dir = Direction.RIGHT;
		if (cmd[4].toLowerCase().equals("left"))
			dir = Direction.LEFT;

		if (cmd[1].equals("jaffa"))
			jaffa = new Player(Actor.Jaffa, new Coordinate(x, y), dir, maze, view);
		if (cmd[1].equals("replicator"))
			replicator = new Replicator(new Coordinate(x, y), dir, maze, view);
		if (cmd[1].equals("colonel"))
			colonel = new Player(Actor.Colonel, new Coordinate(x, y), dir, maze, view);
	}

    public static boolean getOver(){
        return over;
    }

public static void setOver(boolean over){		//checking if the game is over, killing players according to that
        Main.over = over;
		if (over) {
            if (colonel.returnOver())
                maze.setPlayer("colonel", null);
            if (jaffa.returnOver())
                maze.setPlayer("jaffa", null);
            view.repaint();
        }
    }
}
