package com.kollegak.view;

import com.kollegak.model.Maze;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

// Selecting a JFrame
public class View extends JFrame {
	
	private Window window;
	
    public View(Maze maze) throws IOException{
    	
        super("Kollegak");
        getContentPane().setBackground(Color.lightGray);            //Setting background color
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    //Setting action on close
        window = new Window(maze);                                  //Connecting controller
        this.add(window);
    	this.pack();
        this.setVisible(true);
    }
    
    @Override
    public void paint(Graphics g){
    	window.repaint();
    }

    public Window getWindow(){
        return window;
    }
}