package com.kollegak.view;

import com.kollegak.controller.Main;
import com.kollegak.model.Coordinate;
import com.kollegak.model.Maze;
import com.kollegak.model.MazeField;
import com.kollegak.model.Player;
import com.sun.javafx.scene.traversal.Direction;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
// Basic window setup and the pictures used in the graphics
class Window extends JComponent {
    private final int GRID=75;
    private final int wallSize = (int)(GRID * 0.12);
    private int UPMARGIN=0;
    private int LEFTMARGIN=0;
    private final int WIDTH=1024;
    private final int HEIGHT=768;
    private Maze maze;
    private BufferedImage colonel;
    private BufferedImage replicator;
    private BufferedImage jaffa;
    private BufferedImage field;
    private BufferedImage cube;
    private BufferedImage door;
    private BufferedImage opendoor;
    private BufferedImage normalwall;
    private BufferedImage specialwall;
    private BufferedImage pit;
    private BufferedImage cportal;
    private BufferedImage cportal2;
    private BufferedImage jportal;
    private BufferedImage jportal2;
    private BufferedImage scale;
    private BufferedImage zpm;

    private BufferedImage colWin;
    private BufferedImage jafWin;

    Window(Maze maze) throws IOException {

        LEFTMARGIN=(WIDTH-Maze.getSide()*GRID)/2;
        UPMARGIN=(HEIGHT-Maze.getSide()*GRID)/2;

        this.maze=maze;

        //Image reading
        colonel = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/colonel.png"));
        replicator = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/replicator.png"));
        jaffa = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/jaffa.png"));
        field = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/field.png"));
        cube = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/crate.png"));
        normalwall = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/wall.png"));
        specialwall = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/specwall.png"));
        door = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/door.png"));
        opendoor = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/opendoor.png"));
        zpm = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/zpm.png"));
        pit = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/pit.png"));
        cportal = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/cportal.png"));
        cportal2 = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/cportal2.png"));
        jportal = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/jportal.png"));
        jportal2 = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/jportal2.png"));
        scale = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/scale.png"));
        colWin = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/colonel_win.png"));
        jafWin = ImageIO.read(this.getClass().getResourceAsStream("/res/graphics/jaffa_win.png"));
    }

    @Override
    public void paint(Graphics g){

        super.paint(g);

        BufferedImage temp = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = temp.createGraphics();

        drawMaze(g2d);
        drawActors(g2d);
        if (Main.getOver())
            drawVictory(Player.returnWinner(), g2d);

        Graphics2D g2dComponent = (Graphics2D) g;
        g2dComponent.drawImage(temp, null, 0, 0);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(WIDTH, HEIGHT);
    }

    private void drawMaze(Graphics2D g2d){
        for(int i=0; i<Maze.getSide();i++)
            for(int j=0;j<Maze.getSide();j++)
                if(maze.getMazeField(i,j)!=null)
                    drawMazeField(maze.getMazeField(i,j),new Coordinate(i,j),g2d);
    }

    //draw* methods to the components
    private void drawMazeField(MazeField mf, Coordinate pos, Graphics2D g2d){
        drawField(pos, g2d);
        if(mf.getInteractable()!=null)
            drawInteractable(mf.getInteractable().getType(),pos, g2d);
        drawCube(mf.getCube(),pos,g2d);
        drawWall(mf.getWall(Direction.DOWN).getType(),pos, Direction.DOWN, g2d);
        drawWall(mf.getWall(Direction.UP).getType(),pos, Direction.UP, g2d);
        drawWall(mf.getWall(Direction.LEFT).getType(),pos, Direction.LEFT, g2d);
        drawWall(mf.getWall(Direction.RIGHT).getType(),pos, Direction.RIGHT, g2d);
    }

    private void drawField(Coordinate pos, Graphics2D g2d){
        g2d.drawImage(field,
                LEFTMARGIN+pos.getX()*GRID,
                UPMARGIN+pos.getY()*GRID,
                GRID,
                GRID,
                null);
    }

    private void drawInteractable(String type,Coordinate pos, Graphics2D g2d){
        BufferedImage toDraw=null;

        switch (type) {
            case "scale":
                toDraw = scale;
                break;
            case "zpm":
                toDraw = zpm;
                break;
            case "pit":
                toDraw = pit;
        }

        g2d.drawImage(toDraw,
                LEFTMARGIN+pos.getX()*GRID+wallSize,
                UPMARGIN+pos.getY()*GRID+wallSize,
                (GRID-2*wallSize),
                (GRID-2*wallSize),
                null);
    }

    private void drawWall(String type, Coordinate pos, Direction dir, Graphics2D g2d){
        BufferedImage toDraw=null;
        switch (type) {
            case "normalwall":
                toDraw = normalwall;
                break;
            case "specialwall":
                toDraw = specialwall;
                break;
            case "nowall":
                return;
            case "door":
                toDraw = door;
                break;
            case "opendoor":
                toDraw = opendoor;
                break;
            case "portal":
                String portalType=null;
                if (maze.getColonel()!=null)
                    portalType = maze.getColonel().getPortalType(pos, dir);
                if (portalType!=null) {
                    if (portalType.equals("primary"))
                        toDraw = cportal;
                    else
                        toDraw = cportal2;
                }
                else {
                    if (maze.getJaffa()!=null)
                        portalType = maze.getJaffa().getPortalType(pos, dir);
                    if (portalType!=null) {
                        if (portalType.equals("primary"))
                            toDraw = jportal;
                        else
                            toDraw = jportal2;
                    }
                }
                break;
        }

        if (toDraw!=null) {

            AffineTransform at = new AffineTransform();

            int width;
            int height;

            if (dir.equals(Direction.DOWN) || dir.equals(Direction.UP)) {
                width = GRID;
                height = wallSize;
            } else {
                width = wallSize;
                height = GRID;
            }
            BufferedImage newImage = null;

            Coordinate wallpos = new Coordinate();

            if (dir.equals(Direction.UP)) {
                newImage = new BufferedImage(toDraw.getHeight(),toDraw.getWidth(),toDraw.getType());
                at.translate(toDraw.getHeight() / 2, toDraw.getWidth() / 2);
                at.rotate(Math.PI / 2);
                wallpos.setX(pos.getX() * GRID);
                wallpos.setY(pos.getY() * GRID);
            }
            else if (dir.equals(Direction.RIGHT)){
                newImage = new BufferedImage(toDraw.getWidth(),toDraw.getHeight(),toDraw.getType());
                at.translate(toDraw.getWidth() / 2, toDraw.getHeight() / 2);
                at.rotate(2 * (Math.PI / 2));
                wallpos.setX((pos.getX()+1) * GRID - wallSize);
                wallpos.setY(pos.getY() * GRID);
            } else if (dir.equals(Direction.DOWN)) {
                newImage = new BufferedImage(toDraw.getHeight(),toDraw.getWidth(),toDraw.getType());
                at.translate(toDraw.getHeight() / 2, toDraw.getWidth() / 2);
                at.rotate(3 * (Math.PI / 2));
                wallpos.setX(pos.getX() * GRID);
                wallpos.setY((pos.getY()+1) * GRID - wallSize);
            } else if (dir.equals(Direction.LEFT)) {
                newImage = new BufferedImage(toDraw.getWidth(),toDraw.getHeight(),toDraw.getType());
                at.translate(toDraw.getWidth() / 2, toDraw.getHeight() / 2);
                //at.rotate(0 * (Math.PI / 2));
                wallpos.setX(pos.getX() * GRID);
                wallpos.setY(pos.getY() * GRID);
            }
            at.translate(-toDraw.getWidth() / 2, -toDraw.getHeight() / 2);

            AffineTransformOp ato = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
            ato.filter(toDraw, newImage);

            g2d.drawImage(newImage,
                    LEFTMARGIN + wallpos.getX(),
                    UPMARGIN + wallpos.getY(),
                    width,
                    height,
                    null);
        }
    }

    private void drawActors(Graphics2D g2d){
        if(maze.getColonel()!=null)
            drawActor(colonel,maze.getColonel().getDir(),maze.getColonel().getPos(),g2d);
        if(maze.getJaffa()!=null)
            drawActor(jaffa,maze.getJaffa().getDir(),maze.getJaffa().getPos(),g2d);
        if(maze.getReplicator()!=null)
            drawActor(replicator,maze.getReplicator().getDir(),maze.getReplicator().getPos(),g2d);
    }

    private void drawActor(BufferedImage toDraw, Direction dir, Coordinate pos, Graphics2D g2d){

        int i=0;
        if(dir==Direction.DOWN) i=0;
        if(dir==Direction.RIGHT) i=270;
        if(dir==Direction.LEFT) i=90;
        if(dir==Direction.UP) i=180;

        AffineTransform tx = new AffineTransform();
        tx.rotate(Math.toRadians(i), toDraw.getWidth() / 2, toDraw.getHeight() / 2);

        AffineTransformOp op = new AffineTransformOp(tx,
                AffineTransformOp.TYPE_BILINEAR);
        toDraw = op.filter(toDraw, null);

        g2d.drawImage(toDraw,
                LEFTMARGIN+pos.getX()*GRID+wallSize,
                UPMARGIN+pos.getY()*GRID+wallSize,
                (GRID-2*wallSize),
                (GRID-2*wallSize),
                null);
    }

    private void drawCube(int cubeCount,Coordinate pos, Graphics2D g2d){
        if (cubeCount>0)
            g2d.drawImage(cube,
                    LEFTMARGIN+pos.getX()*GRID+GRID/4,
                    UPMARGIN+pos.getY()*GRID+GRID/4,
                    GRID/2,
                    GRID/2,
                    null);
    }

    private void drawVictory(String winner, Graphics2D g2d){
        if (winner.equals("Colonel"))
            g2d.drawImage(colWin,
                    LEFTMARGIN,
                    UPMARGIN,
                    Maze.getSide()*GRID,
                    Maze.getSide()*GRID,
                    null);
        if (winner.equals("Jaffa"))
            g2d.drawImage(jafWin,
                    LEFTMARGIN,
                    UPMARGIN,
                    Maze.getSide()*GRID,
                    Maze.getSide()*GRID,
                    null);
    }
}