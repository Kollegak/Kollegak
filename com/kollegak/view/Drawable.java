package com.kollegak.view;

// Drawable interface
public interface Drawable {
    String getType();
}
