@echo off
echo Running the given tests one by one, please wait for results
java -jar Kollegak.jar test_01
java -jar Kollegak.jar test_02
java -jar Kollegak.jar test_03
java -jar Kollegak.jar test_04
java -jar Kollegak.jar test_05
java -jar Kollegak.jar test_06
java -jar Kollegak.jar test_07
java -jar Kollegak.jar test_08
java -jar Kollegak.jar test_09
java -jar Kollegak.jar test_10
java -jar Kollegak.jar test_11
java -jar Kollegak.jar test_12
java -jar Kollegak.jar test_13
java -jar Kollegak.jar test_14
java -jar Kollegak.jar test_15
java -jar Kollegak.jar test_16
java -jar Kollegak.jar test_17
java -jar Kollegak.jar test_18
java -jar Kollegak.jar test_19
set /p DUMMY=Hit ENTER to Exit.